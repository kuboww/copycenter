var gulp=require('gulp'),
	sass=require('gulp-sass'),
	jade=require('gulp-jade'),
	browserSync=require('browser-sync'),
	concat=require('gulp-concat'),
	uglify=require('gulp-uglify'),
	cssnano=require('gulp-cssnano'),
	rename=require('gulp-rename'),
	del=require('del'),
	imagemin=require('gulp-imagemin'),
    cache=require('gulp-cache'),
    pngquant=require('imagemin-pngquant'),
    autoprefixer=require('gulp-autoprefixer'),
    spritesmith = require('gulp.spritesmith'),
    mainBowerFiles = require('main-bower-files'),
    inject = require('gulp-inject'),
    runSequence = require('run-sequence'), //
    series = require('stream-series');




// inject into jade

gulp.task('injectJS', function() {

    var vendorStreamJS = gulp.src(['./dev/vendor/**/*.js', '!./dev/vendor/**/jquery.js' ], {read: false});
    var   appStreamJS = gulp.src(['./dev/js/**/*.js'], {read: false});
    var vendorjquerystreamJS = gulp.src('./dev/vendor/**/jquery.js', {read: false});

    gulp.src('./dev/*.jade')
        // .pipe(inject(gulp.src('./dev/vendor/**/jquery.js', {read: false}), {name: 'jquery'}, {relative: true})) 
        .pipe(inject(series(vendorjquerystreamJS), {name: 'jquery', relative: true}))
        .pipe(inject(series(vendorStreamJS, appStreamJS), {relative: true})) // This will always inject vendor files before app files
        .pipe(gulp.dest('./dev'))
});



gulp.task('injectCSS', function() {
    var vendorStreamCSS = gulp.src(['./dev/vendor/**/*.css'], {read: false});
    var appStreamCSS = gulp.src(['./dev/css/**/*.css'], {read: false});

    gulp.src('./dev/*.jade')
      .pipe(inject(series(vendorStreamCSS, appStreamCSS), {relative: true})) // This will always inject vendor files before app files
      .pipe(gulp.dest('./dev'))
});


// inject into jade end


 //mainBowerFiles start

gulp.task('mainJS', function() {
    return gulp.src(mainBowerFiles('**/*.js', {
      "overrides": {
        "bootstrap": {
            "main": [
                "./dist/js/bootstrap.min.js"
                ]
        }
    }
}))
    .pipe(gulp.dest('dev/vendor/js'))
});

gulp.task('mainCSS', function() {
    return gulp.src(mainBowerFiles('**/*.css', {
      "overrides": {
        "bootstrap": {
            "main": [
                "./dist/css/bootstrap.min.css"
                
                ]
        }
    }}))
    .pipe(gulp.dest('dev/vendor/css'))
});

//mainBowerFiles  end

// jade to html srart
gulp.task('html', function() {
  var YOUR_LOCALS = {};
 
  gulp.src('./dev/*.jade')
    .pipe(jade({
      locals: YOUR_LOCALS,
      pretty: true
    }))
    .pipe(gulp.dest('./dev/'))
});
// jade to html end


// sass to css
gulp.task('sass', function(){
	return gulp.src('dev/sass/*.scss')
		.pipe(sass())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer(['last 15 version', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
		.pipe(gulp.dest('dev/css'))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function(){
	return gulp.src([
		
		])
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('dev/js'))
});

// jade to html end


// local host
gulp.task('browser-sync', function(){
	browserSync({
		server:{
			baseDir: 'dev'
		},
		notify: true
	});
});
// local host end



// clear start
gulp.task('clean', function() {
    return del.sync('app');
});

gulp.task('clear', function() {
    return cache.clearAll();
});
// clear end



// img compress
gulp.task('img', function() {
    return gulp.src('dev/img/**/*') 
        .pipe(cache(imagemin({
            interlaced: false,
            progressive: false,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('app/img')); 
});
// img compress end



// sprite
gulp.task('sprite', function() {
    var spriteData = 
        gulp.src('dev/img/sprite/*.*') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: '_sprite.css',
                padding: 5,
                imgPath: '../img/sprite.png',
                
            }));

    spriteData.img.pipe(gulp.dest('dev/img/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('dev/sass/')); // путь, куда сохраняем стили
});

// sprite end

// gulp.task('inject',['mainJS', 'mainCSS', 'injectJS', 'injectCSS'], function(){
//
// });


gulp.task('inject', function(done) {
    runSequence('mainJS', 'mainCSS',['injectJS', 'injectCSS'],

        done);

});



// default
gulp.task('watch', ['browser-sync', 'sass', 'scripts', 'html'], function(){
	gulp.watch('dev/sass/*.scss', ['sass']);
	gulp.watch('dev/**/*.jade', ['html']);
	gulp.watch('dev/**/*.html', browserSync.reload);
	gulp.watch('dev/**/**/*.js', browserSync.reload);
	gulp.watch('dev/**/**/*.css', browserSync.reload);
});


// build app
gulp.task('build', ['clean', 'img', 'sass', 'scripts'], function() {
     var vendor = gulp.src('dev/vendor/**/*')
    .pipe(gulp.dest('app/vendor'));

    var buildCss = gulp.src('dev/css/**/*')
    .pipe(gulp.dest('app/css'));

    var buildFonts = gulp.src('dev/fonts/**/*')
    .pipe(gulp.dest('app/fonts'));

    var buildJs = gulp.src('dev/js/**/*')
    .pipe(gulp.dest('app/js'));

    var buildHtml = gulp.src('dev/*.html')
    .pipe(gulp.dest('app'));
});

gulp.task('default', ['watch']);