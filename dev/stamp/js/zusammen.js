function setCookie(e,t,a){a=a||{};var n=a.expires;if("number"==typeof n&&n){var r=new Date;r.setTime(r.getTime()+1e3*n),n=a.expires=r}n&&n.toUTCString&&(a.expires=n.toUTCString()),t=encodeURIComponent(t);var s=e+"="+t;for(var o in a){s+="; "+o;var m=a[o];m!==!0&&(s+="="+m)}document.cookie=s}
function getCookie(e,t){t=t||"";var a=document.cookie.match(new RegExp("(?:^|; )"+e.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g,"\\$1")+"=([^;]*)"));return a=a?decodeURIComponent(a[1]):"",""==a?t:a}
function show_on(e){el=document.getElementById(e),el.setAttribute("display","block"),el.style.display="block"}
function show_off(e){el=document.getElementById(e),el.setAttribute("display","none"),el.style.display="none"}
function price_current_sales(e){current_sales=parseInt(e);for(var t=0;t<sample_count&&!(sample_text[t][1]<=current_sales&&current_sales<=sample_text[t][2]);t++);return t<sample_count?sample_text[t][5]:void 0}
function NametoID(e){for(var t=0;t<names_data.length&&names_data[t][0]!=e;t++);return t}
function Size_to_Path(temp_str){while(temp_str.length<3){temp_str="0"+temp_str;}return temp_str;}
function Min_Max_Price(price_list){if(price_list.length>0){var price_min=parseInt(price_list[0].toString());var price_max=parseInt(price_list[0].toString());}for(var k=1;k<price_list.length;k++){temp_price=parseInt(price_list[k].toString());if(temp_price<price_min){price_min=temp_price;}if(temp_price>price_max){price_max=temp_price;}}price_list[0]=price_min;price_list[price_list.length-1]=price_max;return price_list;}
var sample_text=new Array;
sample_text[0]=["Образцы макетов одноцветных печатей без защиты",1,24,96,"left",142];
sample_text[1]=["Образцы макетов печатей с 1-й степенью защиты",25,28,45,"left",172];
sample_text[2]=["Образцы макетов печатей с 2-я степенями защиты",29,36,45,"right",180];
sample_text[3]=["Образцы макетов печатей с 3-я степенями защиты",37,40,45,"left",231];
sample_text[4]=["Образцы макетов двухцветных печатей",41,50,96,"left",255];
sample_text[5]=["Образцы макетов печатей с логотипом"];
sample_count=4;
stamp_model=["TRODAT 4642","TRODAT 4924","IDEAL 400R","TRODAT 52040","TRODAT 9440"];
var sample_price=new Array;
sample_price[0]=[["4642 GREEN",186],["4642 ARCTIC",168],["4642 WHITE",168],["4642 RED",186],["4642 BLUE",189],["4642 BLACK",189],["4642 GREY",189],["4642 ORANGE",174],["4642 VIOLET",0],["4642 TURQ",186],["4642 PINK",174],["4642 MINT",186],["4642 NEON",168],["4642 NEON",168],["4642 NEON",168]];
sample_price[1]=[["4924 UA",162],["4924 BLAU",0],["4924 BLACK",162],["4924 GREY",156],["4924 BLUE",162],["4924 GREEN",156],["4924 VIOLET",162],["4924 RED",156]];
sample_price[2]=[["400R BLACK",55.75],["400R BLUE",120],["400R GREEN",55.75],["400R RED",55.75]];
sample_price[3]=[["52040 BLACK",141.24]];
sample_price[4]=[["9440 BLUE",63.45],["9440 BLACK",63.45],["9440 RED",63.45],["9440 SILVER",63.45]];
var names_data=new Array;
names_data[0]=["4642","TRODAT","Ø 42 мм",132],names_data[1]=["4924","TRODAT","40 x 40 мм",117],names_data[2]=["400R","IDEAL","Ø 39 мм",110],names_data[3]=["52040","TRODAT","Ø 40 мм",170],names_data[4]=["9440","TRODAT","40 x 40 мм",90];
current_press=0,current_model=0;

stamp_00_04=85;stamp_04_07=90;stamp_07_15=95;stamp_15_20=100;stamp_20_99=160;
stamp_size=[	[26,9,"4910,8910",stamp_00_04,"TRODAT 4910, IMPRINT 10","138,99","70,65","","2,2"],
		[38,14,"4911,8911",stamp_04_07,"TRODAT 4911, IMPRINT 11","144,105","80,75","","10,2"],
		[41,24,"4941,5200",stamp_07_15,"TRODAT 4941, TRODAT 5200","233,573","122,151","","1,1"],
		[47,18,"4912,8912",stamp_07_15,"TRODAT 4912, IMPRINT 12","186,134","89,84","","10,2"],
		[50,30,"4929",stamp_15_20,"TRODAT 4929","295","105","","2"],
		[58,22,"4913,8913",95,"TRODAT 4913, IMPRINT 13","224,162","97,92","","10,2"],
		[60,33,"4928",stamp_15_20,"TRODAT 4928","326","110","","2"],
		[60,40,"4927,5207",147,"TRODAT 4927,TRODAT 5207","330,697","122,170","","4,1"],
		[64,26,"4914",105,"TRODAT 4914","262","98","","2"],
		[70,25,"4915,8915,5205",105,"TRODAT 4915,IMPRINT 15,TRODAT 5205","342,261,683","104,99,158","","2,2,1"],
		[75,38,"4926",stamp_20_99,"TRODAT 4926","371","120","","2"],
		[82,25,"4925",stamp_20_99,"TRODAT 4925","314","95","","1"],
		[116,70,"5212",stamp_20_99,"TRODAT 5212","1212","244","","1"]]
x_size=[26,38,41,47,50,58,60,64,70,75,82];y_size=[9,14,18,22,24,25,26,30,33,38,40];
dater_size=[	[41,24,"4750,5430",stamp_07_15,"TRODAT 4750, TRODAT 5430","469,888","122,151","4,4","1,1"],
		[50,30,"4729",stamp_15_20,"TRODAT 4729","461","105","3","1"],
		[60,40,"4727,5470",stamp_20_99,"TRODAT 4727,TRODAT 5470","669,1074","122,170","4,4","1,1"],
		[75,38,"4726",stamp_20_99,"TRODAT 4726","669","120","4","1"],
		[116,70,"54120",stamp_20_99,"TRODAT 54120","1461","244","4","1"]]
dater_x_size=[41,50,60,75];dater_y_size=[24,30,38,40];order_list=[0,1,3,5,8,9,2,4,6,7,10,11];
dater_list=[0,1,2,3];
stamp_count=stamp_size.length-1;dater_count=dater_size.length-1;start_stamp=5;start_dater=2;it_is_dater=false;names_price=[];

names_price[4910] = [stamp_00_04, "черный, 138", "синий, 138", "красный, 0", "розовый, 0", "зеленый, 0", "серый, 0", "белый, 0"], 
names_price[8910] = [stamp_00_04, "черный, 99", "синий, 99", "красный, 0"], 
names_price[4911] = [stamp_04_07, "черный, 144", "синий, 144", "зеленый, 144", "серый, 144", "красный, 144", "розовый,  144", "белый, 144", "неон оранж, 137", "неон розовый, 137", "неон желтый, 137"], 
names_price[8911] = [stamp_04_07, "черный, 105", "синий, 105", "красный, 0"], 
names_price[4912] = [stamp_07_15, "черный, 186", "синий, 186", "зеленый, 186", "серый, 186", "красный, 186", "розовый,  186", "белый, 186", "неон оранж, 177", "неон розовый, 177", "неон желтый, 177"], 
names_price[8912] = [stamp_07_15, "черный, 134", "синий, 134", "красный, 0"], 
names_price[4913] = [95, "черный, 224", "синий, 224", "зеленый, 224", "серый, 224", "красный, 224", "розовый,  224", "белый, 224", "неон оранж, 213", "неон розовый, 213", "неон желтый, 213"], 
names_price[8913] = [95, "черный, 162", "синий, 162", "красный, 0"], 
names_price[4941] = [stamp_07_15, "Eco черный, 233"], 
names_price[5200] = [stamp_07_15, "Eco черный, 573"], 
names_price[4750] = [stamp_07_15, "Eco черный, 469"], 
names_price[5430] = [stamp_07_15, "Eco черный, 888"], 
names_price[4729] = [stamp_15_20, "красный, 461"], 
names_price[4929] = [stamp_15_20, "синий, 295", "черный, 295", "красный, 0", "серый, 0"], 
names_price[4928] = [stamp_15_20, "синий, 326", "черный, 326", "красный, 0", "серый, 0"], 
names_price[4727] = [stamp_20_99, "красный, 669"], 
names_price[4927] = [147, "синий, 330", "черный, 330", "красный, 330", "серый, 330"], 
names_price[5207] = [147, "Eco черный, 697"], 
names_price[5470] = [stamp_20_99, "Eco черный, 1074"], 
names_price[4914] = [105, "черный, 262", "синий, 262", "красный,0", "розовый,  0", "зеленый, 0", "серый, 0", "белый, 0"], 
names_price[4915] = [105, "черный, 342", "синий, 342", "красный, 0", "розовый,  0", "зеленый, 0", "серый, 0", "белый, 0"], 
names_price[8915] = [105, "черный, 261", "синий, 261", "красный, 0"], 
names_price[5205] = [105, "Eco черный, 683"], 
names_price[4726] = [stamp_20_99, "красный, 669"], 
names_price[4926] = [stamp_20_99, "синий, 371", "черный, 371", "красный, 0", "серый, 0"], 
names_price[4925] = [stamp_20_99, "синий, 314", "черный, 314", "красный, 0", "серый, 0"];
start_farben_price=99;
outer_text = '';
place_holder = "Разместите здесь текст, который Вы хотели бы получить на оттиске штампа, а мы сделаем и вышлем Вам несколько эскизов под наиболее подходящую модель оснастки. Если Вы выберете самостоятельно модель корпуса и цвет краски оттиска, перейдя по ссылке <br><center><strong>Выбрать</strong>,</center>то это значительно ускорит процесс.<br>Спасибо.";
