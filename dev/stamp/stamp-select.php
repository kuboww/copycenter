<?PHP   
  define('bound_text',  uniqid() );
  define('SAMPLE_NAME', 'example_'. bound_text . '.png');
  define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT']."/upload/");
  
  $data = null;
  if (isset($_REQUEST) && (array_key_exists('sales_data', $_REQUEST))){
    $data = $_REQUEST['sales_data'];
  }
//  $price 	=  $_REQUEST['sales_price'];
  
    if ($data) {
  list($settings, $encoded_string) = explode(',', $data);
  list($img_type, $encoding_method) = explode(';', substr($settings, 5));
  
  if($encoding_method == 'base64'){
      $file=fopen(UPLOAD_DIR . SAMPLE_NAME,'w+');
      fwrite($file,base64_decode($encoded_string)) ;
      fclose($file);
  }
     }
?>
<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Создание макета штампа онлайн. Автоматический выбор размера. Подбор цвета оттиска и оснастки. Изготовление и доставка.">
    <meta name="keywords" content="штамп, датер, печать, онлайн, макет, заказать, купить, изготовить, киев, Украина, ТРОДАТ">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="public">
    <meta http-equiv="Cache-Control" content="max-age=34700">

    <title>Выбор оснастки для эскиза штампа</title>

    <link rel="icon" type="image/vnd.microsoft.icon" href="./favicon.ico">
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico">
    <link href="./css/sales.css" rel="stylesheet" type="text/css" media="all">
    <link href="./css/buttons.css" rel="stylesheet" type="text/css" media="all">
    <!--
      <link href="./css/stamp.css" rel="stylesheet" type="text/css" media="all">
      <script type="text/javascript" src="./js/ajaxupload.3.5.js"></script>
    <script src="./js/jquery-1.12.4.min.js" type="text/javascript"></script>
      -->
    <script src="./js/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="./alert/alert.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="./alert/alert.css">
    <script type="text/javascript" src="./js/zusammen.js"></script>
    <script type="text/javascript" src="./js/stamp-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./slick/slick.css">
    <link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
    <link href="./css/select.css" rel="stylesheet" type="text/css" media="all">
</head>

<body>
    <div>
        <div class="example" style="width: 460px;float:right;">
            <canvas height="265" width="439" id="clishe" style="float: right;padding:0 5px 10px 0;"></canvas>
        <?PHP
          echo'   <script type="text/javascript">';
          echo'	var clishe = document.getElementById("clishe");';
          echo'	var ctx_clishe = clishe.getContext("2d");';
          
          echo'	var price_clishe = 0;';
          
          if (SAMPLE_NAME){
          echo'	img_clishe.src ="/upload/'. SAMPLE_NAME .'";';
          }else{
          echo'	img_clishe.src = "/upload/test_stamp.png"';
          }
          echo'	ctx_clishe.drawImage(img_clishe, 0, 0, 439,265);';
          
          echo'	         </script>';
          
          ?>
            <script type="text/javascript">
                document.write('<div class="text_for_string block" style="float:right;width:405px;height:120px;">');
                document.write('<div class="block-head">');
                document.write('<h3>Выберите цвет сменной подушки для оттиска<br></h3>');
                document.write('</div>');
                document.write('<section class="variable pads slider_slick" id="all_pads">');
                add_to_class = ' active';
                add_to_style = 'margin-left:1px;';
                temp_color = ['', 'blue', 'violet', 'red', 'black', 'green', 'dry'];
                for (var i = 1; i < 7; i++) {
                    temp_str = "0" + i.toString()
                    if (i > 1) add_to_class = add_to_style = '';
                    document.write('<div><img class="pads_img' + add_to_class + '" id="' + temp_str + '" src="./images/stamp/pads/pads_' + temp_str + '.png" style="' + add_to_style + '">');
                    document.write('<span class="farben_name">' + temp_color[i] + '</span></div>');
                }
                document.write('</section>');
                document.write('</div>');

                document.write('<div class="text_for_string block" style="float:right;width:405px;height:125px;">');
                document.write('<div class="block-head">');
                document.write('<h3>Или выберите особый цвет краски<br></h3>');
                document.write('</div>');
                document.write('<section class="variable farben slider_slick" id="all_farben">');
                //		                 add_to_class=' active';
                temp_color = ['carmin-red', 'deep-orange', 'light-blue', 'orange-brown', 'signal-yellow', 'sky-blue', 'squirl-grey', 'traffic-purpur', 'yellow-green', 'zinc-yellow'];
                for (var i = 0; i < 10; i++) {
                    temp_str = temp_color[i];
                    if (i > 0) add_to_class = '';
                    document.write('<div><img class="farben_img' + add_to_class + '" id="' + temp_str + '" src="./images/sales/farben/7012/7012-' + temp_str + '.png">');
                    document.write('<span class="farben_name">' + temp_str.replace("-", "<br>") + '</span></div>');
                }
                document.write('</section>');
                document.write('</div>');
            </script>
        </div>
        <div style="float:left;width:550px;">
            <canvas height="300" width="300" id="stamp" style="float:left;padding: 0 0 30px 50px;" border=1></canvas>
        <?PHP
          echo'   <script type="text/javascript">';
          echo'	var stamp = document.getElementById("stamp");';
          echo'	var ctx_stamp = stamp.getContext("2d");';
          echo'	         </script>';
          ?>
            <form id="step2" action="checker.php" method="post" enctype="multipart/form-data">
                <input name="color_clishe" type="hidden" id="id_color_clishe" value="">
                <button class="zuruck" id="stamp_zuruck" style="position: relative; top: 50px; left: 50px;">ИЗМЕНИТЬ
                    <br>МАКЕТ</button>
                <button type="submit" id="complete_order" style="position: relative; top: 100px; left: 50px;" form="step2" formaction="checker.php" formmethod="post" class="finish" onclick="next_step_2();">
			ОФОРМИТЬ<br>ЗАКАЗ</button>
            </form>
        </div>
        <div style="padding-bottom: 16px" id="calculation">
            <div class="price_cont">
                Стоимость клише:
                <div class="smeta" id="clishe">
                    <span class="price_value">
              <script type="text/javascript">document.write(price_clishe);</script>
            </span> грн.
                </div>
            </div>
            <div class="price_cont">
                Стоимость оснастки:
                <div id="osnastka" class="smeta"><span id="price_sales">-</span> грн.</div>
            </div>
            <div class="price_cont">
                Стоимость краски:
                <div id="farben" class="smeta"><span id="price_farben">-</span> грн.</div>
            </div>
            <div class="price_total">
                Итого:
                <div class="smeta_total" id="total"><span id="price_total">МНОГО</span><font color="#285470"> грн.</font>
                </div>
            </div>
        <script type="text/javascript">
	  if (getCookie("place_holder",'').length>0) {
		document.write('<div style="width: 500px;color: crimson; font: 600 12px/14px Arial,Verdana,sans-serif;">Внимание! Точная стоимость оснастки с клише станет известна после создания макета дизайнером и выбора оснастки под размер оттиска.</div>')
	  } else { $('#calculation').css('padding-bottom','44px');
	  }
        </script>
        </div>
    </div>
    <div>
        <script type="text/javascript">
            for (var j = 0; j < stamp_count; j++) {

                picture_list = stamp_size[j][2].split(",");
                picture_path = Size_to_Path("" + stamp_size[j][0]) + "_" + Size_to_Path("" + stamp_size[j][1]);
                //			  if ((j == start_stamp)&&!(it_is_dater))?this_view="block":this_view="none";
                this_view = "none";
                name_list = stamp_size[j][4].split(",");
                price_list = stamp_size[j][5].split(",");
                size_list = stamp_size[j][6].split(",");

                new_size_list = Min_Max_Price(size_list);
                size_max = 95 / new_size_list[new_size_list.length - 1];

                my_parent = j.toString();
                if (j < 10) {
                    my_parent = "0" + my_parent;
                }

                document.write('<div class="text_for_string block" style="float:left;width:96%;height:235px;display:' + this_view + ';" id="s_' + picture_path + '">');
                document.write('<h3>Модели автоматических оснасток для размера штампа ' + stamp_size[j][0] + ' мм х ' + stamp_size[j][1] + ' мм</h3>');
                document.write('<section class="variable classik slider_slick" id="stamp_' + picture_path + '">');
                add_to_class = " active";
                add_to_style = "margin-left:2px;"
                for (var i = 0; i < picture_list.length; i++) {
                    temp_str = picture_list[i].toString();
                    temp_size = size_list[i] * size_max;
                    my_num = i.toString();
                    if (i < 10) {
                        my_num = "0" + my_num;
                    }
                    if (i > 0) {
                        add_to_style = add_to_class = '';
                    }
                    document.write('<div><img id="' + temp_str + '" class="stamp_img' + add_to_class + '" src="./css/images/zero.png" border=0 style="background: url(./images/stamp/' +
                        picture_path + '/' + temp_str + '/' + temp_str + '_01.png' + ') center bottom no-repeat; background-size: auto ' + parseInt(temp_size) + '%;' + add_to_style + '" >');
                    document.write('<div class="stamp_name"><center>' + name_list[i].toString() + ', &nbsp; ' + price_list[i].toString() + ' грн</center></div>');
                    document.write('</div>');
                }

                document.write('</section>');
                document.write('</div>');



                for (var i = 0; i < picture_list.length; i++) {

                    color_list = stamp_size[j][8].split(",");

                    temp_str = picture_list[i].toString();

                    my_parent = i.toString();
                    if (i < 10) {
                        my_parent = "0" + my_parent;
                    }

                    add_to_style = "margin-left:2px;"
                    add_to_class = " active";

                    this_view = "none";
                    document.write('<div class="text_for_string block" style="float:left;width:96%;height:235px;display:' + this_view + ';" id="c_' + temp_str + '">');
                    document.write('<h3>Модели вариантов цветов корпуса для автоматической оснастки ' + name_list[i].toString() + '</h3>');
                    document.write('<section class="variable color_variant slider_slick" id="' + picture_path + '">');

                    for (var k = 1; k <= parseInt(color_list[i]); k++) {
                        my_num = k.toString();
                        if (k < 10) {
                            my_num = "0" + my_num;
                        }
                        full_path = './images/stamp/' + picture_path + '/' + temp_str + '/' + temp_str + '_' + my_num + '.png'
                        document.write('<div><img id="' + full_path + '" class="color_img ' + temp_str + ' ' + my_num + ' ' + add_to_class + '" src="./css/images/zero.png" border=0 style="background: url(' +
                             full_path + ') center bottom no-repeat; background-size: contain;' + add_to_style + '" >');
                        document.write('<div class="stamp_name"><center>' + names_price[temp_str][k] + ' грн</center></div>');
                        document.write('</div>');

                        if (k > 0) add_to_style = add_to_class = '';
                    }

                    document.write('</section>');
                    document.write('</div>');

                }

            }
        </script>
    </div>
    <div>
        <script type="text/javascript">
            for (var j = 0; j < dater_count; j++) {

                picture_list = dater_size[j][2].split(",");
                picture_path = Size_to_Path("" + dater_size[j][0]) + "_" + Size_to_Path("" + dater_size[j][1]);
                this_view = "none";
                name_list = dater_size[j][4].split(",");
                price_list = dater_size[j][5].split(",");
                size_list = dater_size[j][6].split(",");

                my_parent = j.toString();
                if (j < 10) {
                    my_parent = "0" + my_parent;
                }

                new_size_list = Min_Max_Price(size_list);
                size_max = 95 / new_size_list[new_size_list.length - 1];


                document.write('<div class="text_for_string block" style="float:left;width:96%;height:235px;display:' + this_view + ';" id="d_' + picture_path + '">');
                document.write('<h3>Модели автоматических оснасток для размера датера ' + dater_size[j][0] + ' мм х ' + dater_size[j][1] + ' мм</h3>');
                document.write('<section class="variable classik slider_slick" id="d_' + picture_path + '">');
                add_to_class = " active";
                add_to_style = "margin-left:10px;"
                for (var i = 0; i < picture_list.length; i++) {
                    temp_str = picture_list[i].toString();
                    temp_size = size_list[i] * size_max;
                    my_num = i.toString();
                    if (i < 10) {
                        my_num = "0" + my_num;
                    }
                    if (i > 0) add_to_style = add_to_class = '';
                    document.write('<div><img id="' + temp_str + '" class="stamp_img' + add_to_class + '" src="./css/images/zero.png" border=0 style="background: url(./images/stamp/' +
                        picture_path + '/' + temp_str + '/' + temp_str + '_01.png' + ') center bottom no-repeat; background-size: auto ' + parseInt(temp_size) + '%;' + add_to_style + '" >');
                    document.write('<div class="stamp_name"><center>' + name_list[i].toString() + ', &nbsp; ' + price_list[i].toString() + ' грн</center></div>');
                    document.write('</div>');
                }

                document.write('</section>');
                document.write('</div>');


                for (var i = 0; i < picture_list.length; i++) {

                    color_list = dater_size[j][8].split(",");

                    temp_str = picture_list[i].toString();

                    my_parent = i.toString();
                    if (i < 10) {
                        my_parent = "0" + my_parent;
                    }

                    add_to_style = "margin-left:10px;"
                    add_to_class = " active";

                    this_view = "none";
                    document.write('<div class="text_for_string block" style="float:left;width:96%;height:235px;display:' + this_view + ';" id="c_' + temp_str + '">');
                    document.write('<h3>Модели вариантов цветов корпуса для автоматического датера ' + name_list[i].toString() + '</h3>');
                    document.write('<section class="variable color_variant slider_slick" id="' + picture_path + '">');

                    for (var k = 1; k <= parseInt(color_list[i]); k++) {
                        my_num = k.toString();
                        if (k < 10) {
                            my_num = "0" + my_num;
                        }
                        full_path =  './images/stamp/' + picture_path + '/' + temp_str + '/' + temp_str + '_' + my_num + '.png'
                        document.write('<div><img id="' + full_path + '" class="color_img ' + temp_str + ' ' + my_num + ' ' + add_to_class + '" src="./css/images/zero.png" border=0 style="background: url(' +
                            full_path + ') center bottom no-repeat; background-size: contain;' + add_to_style + '" >');
                        document.write('<div class="stamp_name"><center>' + names_price[temp_str][k] + ' грн</center></div>');
                        document.write('</div>');

                        if (k > 0) add_to_style = add_to_class = '';
                    }

                    document.write('</section>');
                    document.write('</div>');

                }



            }
        </script>
    </div>

    <span style="color: #888;">
      <center>
        Цвет оттиска печати, как и цвет оснастки может не полностью соответствовать указанным изображениям из-за сжатия, применяемого в сети Интернет.
      </center>
    </span>

    <script src="./slick/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
 	clishe = document.getElementById("clishe");
 	ctx_clishe = clishe.getContext("2d");
        PopAllCookie();
        re_draw();
    </script>
</body>

</html>